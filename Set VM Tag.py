    """Assign tags to a machine

    :param inputs
    :param inputs.tags: The initial tags for the machine, supplied from the event data
           during actual provisioning or from user input for testing purposes.
    :param inputs.newTags: The tags to assign to the machine, which can be
           in the form {"name": "value"} for name=value tags
           or {"name": ""} for a simple name tag.
    :return The desired tags.
    """
exports.handler = function handler(context, inputs) {
    let newTags = inputs.new_tags;
    let outputs = {};
    outputs.tags = inputs.tags;

    outputs.tags["UserName"] = inputs._metadata.userName;
    outputs.tags["EventTopic"] = inputs._metadata.evenTopicId;
        
    object.keys(newTags).forEach(key => {
        outputs.tags[key] = newTags[key];
    });
    console.log("Setting tags: " + JSON.stringify(newTags));
    return outputs;
        
};
  